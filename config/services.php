<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => '268434560204132',
        'client_secret' => '0cf25fb5b60a1e00973fec8e3f4acbdc',
        'redirect' => 'http://localhost:8888/auth/facebook/callback',
    ],
    'google' => [
        'client_id' => '93762141640-h9n8nsprs0p3ggqtrqdo6ai3t3f5n4cq.apps.googleusercontent.com',
        'client_secret' => 'tC1Ap3fhTyoYZHajogLpr8XM',
        'redirect' => 'http://localhost:8888/auth/google/callback'
    ],
    'twitter' => [
        'client_id' => 'g1RIm2AqZKetVtps6l95ivkR5',
        'client_secret' => 'KbI6PlHHI0izIpHdDCgBvYwCJIHtS9WA5q2UFyhreaZb9o81sc',
        'redirect' => 'http://localhost:8888/auth/twitter/callback'
    ],
    'linkedin' => [
        'client_id' => '787cmvz5wqqd2q',
        'client_secret' => 'gExkDLPynHgAoBbI',
        'redirect' => 'http://localhost:8888/auth/linkedin/callback'
    ],
//    'bitbucket' => [
//        'client_id' => 'f5GdKdckpLznERh3Hv',
//        'client_secret' => 'LpHDG9a8ejZW33FYdGG4MQHpHMBS8YnU',
//        'redirect' => 'http://localhost:8888/auth/bitbucket/callback'
//    ],
];
