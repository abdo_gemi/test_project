@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-lg-offset-2">
            	<div class="panel panel-collapse">
            		<div class="panel-body">
                        <h3>The Posts with tag <a href="#">
                                <kbd class="btn-primary">{{$tag->name}}</kbd>
                            </a></h3>
            		</div>
                    @each('post.includes.unit',$tag->posts_p,'post')
            	</div>
            </div>
         </div>
    </div>
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-lg-offset-4">
                {{$tag->posts_p->links()}}
        </div>
    </div>
@endsection