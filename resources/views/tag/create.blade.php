@extends('layouts.app')
@section('title','')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                	  <div class="panel-heading">
                			<h3 class="panel-title">Add new Tags</h3>
                	  </div>
                	  <div class="panel-body">
                          {!! Form::open(['class'=>'form-horizontal', 'action'=>['TagController@store']]) !!}
                          {!! Form::bsText('name',null,'Enter Tag name')!!}
                          <div class="form-group">
                              <div class="col-sm-offset-4 col-sm-10">
                                  {!! Form::submit('Add new Catalogue',['class'=>'btn btn-pirmary']) !!}
                              </div>
                          </div>
                          {!! Form::close() !!}
                	  </div>
                </div>

                </div>
            </div>
        </div>
    </div>
@endsection
