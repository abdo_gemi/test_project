<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>BitPost @yield('title')</title>

    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="{{ asset('src/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('src/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('src/css/bootstrap-theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('src/css/bootstrap-theme.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('src/css/select2.min.css')}}" />
    <!-- JavaScripts -->
    <script src="{{ asset('src/js/bitPosts.js')}}"></script>
    <script src="{{ asset('src/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('src/js/tested.js')}}"></script>
    <script src="{{ asset('src/js/jquery.js')}}"></script>
    <script src="{{ asset('src/js/select2.min.js')}}"></script>
    <!-- Styles -->
    <style>
        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout"    style=" background-color: #abd5e8;">
    @include('layouts.menu.main')
    @yield('content')
</body>
</html>
