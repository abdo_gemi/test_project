<div class="row">
    <div class="col-xs-3 col-xs-offset-1">
        <div class="panel panel-collapse">
            <div class="panel-heading">
                <h3 class="panel-title">{{$user->name}}</h3>
                <hr>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-xs-12">
                        <ul class="img-list">
                            <li>
                                @if(Auth::user()->email===$user->email)
                                    <img class="img-thumbnail img-responsive" src="/{{($user->path_image)}}"
                                         alt=""
                                         id="profile-uploaded">
                                    <div class="text-content">
                                        <center><a style="color: white; text-decoration: none"
                                                   onclick="$('#update-image').trigger('click')"><span
                                                        class="glyphicon glyphicon-picture"></span> Upload photo</a>
                                        </center>
                                    </div>
                                @else
                                    <img class="img-thumbnail img-responsive" src="/{{($user->path_image)}}">
                                @endif
                            </li>
                        </ul>
                        <input type="file" id="update-image" onchange="uploadImageProfile(this)"
                               name="profile_upload" accept="image/*"
                               style="display: none"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-11 col-xs-offset-1">
                        <br><br>
                        <p><b>First name</b> : {{$user->first_name}}</p>
                        <p><b>Last name :</b> {{$user->last_name}}</p>
                        <p><b>E-Mail : </b><a href="#">{{$user->email}}</a></p>
                        <p><b>Posts:</b><span class="badge"> {{count($user->posts)}}</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div class="page-header">
            <h1>My posts
                <small>Feel Free</small>
            </h1>
        </div>
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xs-offset-10">
                <a href="{{asset('post/create')}}" class="btn btn-primary"><span
                            class="glyphicon glyphicon-plus"></span>New post</a>
            </div>
        </div>
        <br>
        @if(isset($user->posts)&& count($user->posts)>0)
            @each('post.includes.unit',$user->posts_p,'post')
        @else
            <h3>You not make post yet
                <hr>
            </h3>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-lg-offset-4">
        {{$user->posts_p->links()}}
    </div>
</div>
@if(Auth::user()->email===$user->email)
    <script src="{{asset('src/js/image_operation.js')}}"></script>
@endif