@extends('layouts.app')

@section('content')
    {{--<div class="panel panel-default">--}}
        {{--<div class="panel-body">--}}
            {{--<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">--}}
                {{--<div class="thumbnail">--}}
                    {{--<button type="button" class="close unload" onclick=" $(this).parent().hide()">&times;</button>--}}
                    {{--<img src="{{asset('src/img/default.png')}}" class="img-thumbnail" id="uploaded-img" alt="Image">--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">--}}
                {{--{!! Form::open(['url'=>'media','files'=>true,'id'=>'upload-img','method'=>'DELETE']) !!}--}}
                {{--{!! Form::file('image',['name'=>'img','type'=>'image','id'=>'i' ])!!}--}}
                {{--{!! Form::close() !!}--}}
                {{--<p id="test"></p>--}}
                {{--<script>--}}
                    {{--$('#i').change(function (e) {--}}
                        {{--var data = new FormData();--}}
                        {{--$.each(e.target.files, function (k, value) {--}}
                            {{--data.append('img', value);--}}
                        {{--});--}}
                        {{--$.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});--}}
                        {{--$.ajax({--}}
                                    {{--url: '{{url('media')}}',--}}
                                    {{--type: 'POST',--}}
                                    {{--contentType: false,--}}
                                    {{--processData: false,--}}
                                    {{--dataType: 'json',--}}
                                    {{--data: data,--}}
                                {{--}--}}
                        {{--).done(function (json) {--}}
                            {{--console.dir(json.image);--}}
                            {{--$('#uploaded-img').attr({'src': "/" + json.image.path_file + '/' + json.image.file_name});--}}
                            {{--window.onbeforeunload = function (e) {--}}
                                {{--document.getElementById('test').innerHTML = "ON BEFORE unload";--}}
                                {{--return false;--}}
                            {{--};--}}
                            {{--window.onunload = function (e) {--}}
                                {{--document.getElementById('test').innerHTML += "ON  unload";--}}
                            {{--}--}}
                        {{--}).fail(function (xhr, status, errorThrown) {--}}
                            {{--alert("Sorry, there was a problem!");--}}
                        {{--});--}}
                    {{--});--}}
                {{--</script>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <div class="thumbnail">
                    <button type="button" class="close" onclick="UnloadImage(this,5)">&times;</button>
                    <p id="test"></p>
                    <img src="{{asset('src/img/default.png')}}" class="img-thumbnail" id="uploaded-img" alt="Image">
                </div>
                <script>
                    function UnloadImage(object,img_id) {
                        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                        $.ajax(
                                {
                                    url: '{{asset('media')}}/' + img_id,
                                    type: 'DELETE',
                                    processData: false,
                                    dataType: 'json',
                                    data: {},
                                }
                        ).done(function (json) {
                            console.dir(json);
                            object.parentElement.hide();
                        }).fail(function (xhr, status, errorThrown) {
                            alert("Sorry, there was a problem!");
                            console.log("Error: " + errorThrown);
                            console.log("Status: " + status);
                            console.dir(xhr);
                        });
                    }
                </script>
            </div>
        </div>
    </div>
@endsection