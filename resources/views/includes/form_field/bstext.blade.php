<div class="form-group {{ $errors->has($name) ? ' has-error' : '' }}">
    @if(isset($label)&&!is_null($label))
    <label for="{{$name}}" class="col-md-4 control-label">{{$label}}</label>
    @endif
    <div class="col-md-6">
        {!! Form::text($name,old($name),array_merge(['class' => 'form-control'], $attributes)) !!}
        @if ($errors->has($name))
            <span class="help-block"><strong>{{ $errors->first($name) }}</strong></span>
        @endif
    </div>
</div>