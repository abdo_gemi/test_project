@if(session('done'))
    <div class="container"> <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{session('done')['title']}}</strong>{{session('done')['body']}}
        </div></div>
@endif