<div class="panel panel-primary" id="parant-post">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-7">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <img class="img-circle img-thumbnail icon_img" src="{{asset($post->user->path_image)}}"/>
                </div>
                <br>
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <h3 class="panel-title"><a href="{{asset('profile/?email='.$post->user->email)}}">{{$post->user->name}}</a>
                        @<a class="btn-primary"
                            href="{{asset('catalogue/'.$post->catalogue->id)}}">{{$post->catalogue->name}}</a>
                    </h3></div>
            </div>
            <div class="col-md-4">
                <p>{{$post->created_at->toCookieString()}}</p>
            </div>
            @if($post->ismy())
                <div class="col-md-1">
                    <div class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <span class="glyphicon glyphicon-pencil" style="color:white" title="Setting"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{asset('post/'.$post->id.'/edit')}}">Edit</a></li>
                            <li id="catalogue_delete">
                                {!! Form::open(['action'=>['PostController@destroy',$post->id]
                                ,'method'=>'DELETE','id'=>'delete'.$post->id]) !!}
                                {!! Form::close() !!}
                                <a data-toggle="modal" data-target="#myModal"
                                   onclick="deletePost('{{$post->id}}')" id="button">Delete</a>
                            </li>
                        </ul>
                    </div>
                </div>
            @endif
            {{--continus--}}
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            @if(isset($post->media_id)&&!is_null($post->media_id))
                <div class="">
                    <blockquote>
                        {{$post->body}}
                    </blockquote>
                    <div class="thumbnail">
                        <a data-toggle="modal" href="#img-model" id="show-post-model"> <img id="show-post-model" class="img-rounded" src="/{{$post->media->full_path}}"/></a>
                    </div>
                </div>
            @else
                {{$post->body}}
            @endif
            <div class="col-lg-5 col-md-10">
                @if(isset($post->tags )&&count($post->tags )>0)
                    @foreach($post->tags as $tag)
                        <a href="{{asset('/tag/'.$tag->name)}}">
                            <kbd class="btn-primary">{{$tag->name}}</kbd>
                        </a>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

</div>