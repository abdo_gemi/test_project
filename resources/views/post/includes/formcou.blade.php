<script src="{{asset('src/js/image_operation.js')}}">
</script>
<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-md-offset-2">
    @if(isset($update)&&isset($post))
        {!! Form::model($post,['class'=>'form-horizontal', 'action'=>['PostController@update',$post->id],'id'=>'postForm','method'=>'PATCH']) !!}
    @else
        {!! Form::open(['class'=>'form-horizontal', 'action'=>['PostController@store'],'id'=>'postForm']) !!}
    @endif
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-title col-md-9 ">
                <h4>{{isset($update)&&isset($post)?'Editing Post':'New Post'}}</h4>
            </div>
            <div class="form-group{{ $errors->has('catalogue_id') ? ' has-error' : '' }}">
                <div class="col-md-3">
                    @if(isset($select_default))
                        {!! Form::select('catalogue_id',App\Models\Catalogues::pluck('name','id'),$select_default
                       ,['class'=>'form-control']) !!}
                    @else
                        {!! Form::select('catalogue_id',App\Models\Catalogues::pluck('name','id'),old('catalogue_id')
                        ,['class'=>'form-control']) !!}
                    @endif
                    @if ($errors->has('catalogue_id'))
                        <span class="help-block"><strong>{{ $errors->first('catalogue_id') }}</strong></span>
                    @endif
                </div>
                <br>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                {!! Form::textarea('body',old('body') ,['class'=>'form-control',
                'id'=>'body','placeholder'=>'Enter what you think?']) !!}
                @if ($errors->has('body'))
                    <span class="help-block"><strong>{{ $errors->first('body') }}</strong></span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                <label for="tags" class="col-md-1 control-label">Tags:</label>
                @if(isset($update)&&isset($post))
                    {!! Form::select('tags[]',App\Models\Tags::pluck('name','id'),$post->tags->pluck('id')->toArray(),['class'=>'form-control','id'=>'tags','multiple']) !!}
                @else
                    {!! Form::select('tags[]',App\Models\Tags::pluck('name','id'),old('tags'),['class'=>'form-control','id'=>'tags','multiple']) !!}
                @endif
                @if ($errors->has('tags'))
                    <span class="help-block"><strong>{{ $errors->first('tags') }}</strong></span>
                @endif
            </div>
            <div class="panel-footer" id="uploaded-img">
                @if(isset($update)&&isset($post)&&isset($post->media))
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="thumbnail">
                            <button type="button" class="close" onclick="UnloadImage(this,'{{$post->media->id}}')">×</button>
                            <img src="/{{$post->media->full_path}}" class="img-thumbnail"
                                 alt="Uploaded image" id="uploaded-img" accept="image/*"></div>
                    </div>
                @else
                    <input type="file" class="btn-file" name="img" id="button_upload"
                           onchange="UploadImage(this)" accept="image/*"/>
                @endif
            </div>
        </div>
        {{--Final footer --}}
        <div class="panel-footer">
            <div class="form-group">
                <div class="col-sm-10">
                    {!! Form::submit('Done',['class'=>'btn btn-']) !!}
                </div>
            </div>
            <br>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<script>
    $('#tags').select2(
            {
                placeholder: "Enter your tags",
                allowClear: true,
                tags: true,
                createTag: function (params) {
                    var term = $.trim(params.term);
                    if (term === '') {
                        return null;
                    }
                    return {
                        id: term,
                        text: term,
                    }
                },
            }
    );
</script>