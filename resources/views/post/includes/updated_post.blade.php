 <div id="queue_posts">
        @if(count($posts)<1)
            <h1>Not his the posts</h1>
        @else
            @each('post.includes.unit',$posts,'post')
        @endif
    </div>
<script type="application/javascript">
    function deletePost(id) {
        document.getElementById('delete_button').setAttribute('onclick', 'document.getElementById(\'delete' + id + '\').submit()');
    }
</script>
<div class="row">
    <div class="col-xs-4 col-lg-offset-4">
        <a class="btn btn-block btn-primary" id="more"><span
                    class="glyphicon glyphicon-plus-sign"></span><b>Show More</b><span class="badge"
                                                                                       id="number-post">{{count($posts)}}</span></a>
    </div>
</div>
<script>
    /**************************************************************************/
    var paginateJson = null;
    $('a#more').click(function () {
        if (!paginateJson) {
            var data = {
                p: "2"
            }
        } else {
            var data = {
                p: paginateJson.currentPage + 1
            }
        }
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
                    url: '/api/posts',
                    type: 'GET',
                    dataType: 'json',
                    data: data
//                        statusCode: {}
                }
        ).done(function (json) {
            for (var i = 0; i < json.data.length; i++) {
                $('div#queue_posts').append(json.data[i]);
            }
            if (json.data.length < 3) {
                $('#more').css('display', 'none');
            }
            paginateJson = json.paginate;
        }).fail(function (xhr, status, errorThrown) {
            alert("Sorry, there was a problem!");
        });
    });
    $('a#show-post-model').click(function (event) {
        var me = event.target;
        $('#post-img-model').html(me.outerHTML);
        var main = (me).closest('div#parant-post');
        $('#post-body-model').html(main.outerHTML);
    });
</script>
<div class="modal" id="img-model" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document" style="">
        <div class="modal-content">
            <div class="modal-body" id="post-model-body" style="background-color: transparent">
                <a type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></a>
                <div class="row">
                    <div class="col-xs-12" id="post-body-model">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>