@extends('layouts.app')
@section('title','Editing')
@section('content')
    <div class="container">
        <div class="row">
            @include('post.includes.formcou',['update'=>'done'])
        </div>
    </div>
@endsection