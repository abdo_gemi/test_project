@extends('layouts.app')
@section('title','Create new Post')
@section('content')
    <div class="container">
        <div class="row">
            @include('post.includes.formcou')
        </div>
    </div>
@endsection
