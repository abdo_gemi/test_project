@extends('layouts.app')

@section('content')
    @include('includes.alert')
    <div class="container">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-md-offset-10">
                <a class="btn btn-primary" href="{{asset('post/create')}}">
                    <span class="glyphicon glyphicon-plus"></span> new Post</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3">
                @include('post.includes.updated_post')
            </div>
        </div>
    </div>
@endsection