@if(isset($form)&&$form=='update'&&isset($catalogue))
    {!! Form::model($catalogue,['method'=>'PATCH','class'=>'form-horizontal',
    'action'=>['CatalogueController@update',$catalogue->id]]) !!}
@else
    {!! Form::open(['class'=>'form-horizontal', 'action'=>['CatalogueController@store']]) !!}
@endif
{!! Form::bsText('name',null,'Enter the name',[]) !!}
<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label for="description" class="col-md-4 control-label">Description</label>

    <div class="col-md-6">
        {!! Form::textarea('description',old('description') ,['class'=>'form-control','id'=>'description']) !!}
        @if ($errors->has('description'))
            <span class="help-block"><strong>{{ $errors->first('description') }}</strong></span>
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-4 col-sm-10">
        {!! Form::submit('Add new Catalogue',['class'=>'btn btn-pirmary']) !!}
    </div>
</div>
{!! Form::close() !!}