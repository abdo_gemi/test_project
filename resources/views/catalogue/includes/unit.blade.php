<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
    <div class="panel panel-info">
        <div class="panel-heading main">
            <h3 class="panel-title">
                <a href="{{asset('/catalogue/'.$catalogue->id)}}"
                   class="btn-block hover">{{$catalogue->name}} <span class="badge">{{$catalogue->posts()->count()}}</span> </a></h3>
            {{--Tools--}}
            <div class="main-content" style="float: right;">
                <div class="dropdown" style="float: right;margin-top: -17px;">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <span class="glyphicon glyphicon-pencil" title="Setting"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{asset('catalogue/'.$catalogue->id.'/edit')}}">Edit</a></li>
                        <li id="catalogue_delete">
                            {!! Form::open(['action'=>['CatalogueController@destroy',$catalogue->id]
                            ,'method'=>'DELETE','id'=>'delete'.$catalogue->id]) !!}
                            {!! Form::close() !!}
                            <a data-toggle="modal" data-target="#myModal"
                               onclick="deleteCatalogue('{{$catalogue->id}}')" id="button">Delete</a>
                        </li>
                        <li id="catalogue_delete"></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel-body">
            {{$catalogue->description}}
        </div>
    </div>
</div>
