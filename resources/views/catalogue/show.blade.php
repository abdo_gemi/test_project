@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">
                            {{$catalogue->name}}
                        </h1>
                    </div>
                    <div class="panel-body">
                        {{$catalogue->description}}
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6">
                <div class="col-xs-1 col-xs-offset-9">
                    <a class="btn btn-primary" href="{{asset('catalogue/createpost/'.$catalogue->id)}}"><span
                                class="glyphicon glyphicon-plus"></span>Add new Post</a>
                </div>
                <div class="row"></div>
                <div class="panel panel-collapse">
                    @include('post.includes.updated_post',['posts'=>$catalogue->posts_p])
                </div>
            </div>
        </div>
    </div>
@endsection