@extends('layouts.app')

@section('content')
    @include('includes.alert')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-10   ">
                <a class="btn btn-primary" href="{{asset('catalogue\create')}}">
                    <span class="glyphicon glyphicon-plus" title="Add Catalogue"></span>
                    Add new Catalogue
                </a>
            </div>
        </div>
        <br>
        <div class="row">
            @each('catalogue.includes.unit',$catalogs,'catalogue')
        </div>

    </div>
    <script type="application/javascript">
        function deleteCatalogue(id) {
            document.getElementById('delete_button').setAttribute('onclick', 'document.getElementById(\'delete' + id + '\').submit()');
        }
    </script>
@endsection