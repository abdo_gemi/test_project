<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Input::get('email')) {
            $user = (User::with('posts')->with('media')->where('email', Input::get('email'))->first());
            if (!isset($user)) return response('Not found page', 404);
            return view('profile.index',['user'=>$user]);
        }else{
            return view('profile.index',['user'=>Auth::user()]);
        }
    }

}
