<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Posts;
use App\Models\Tags;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    /**
     * PostController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('post.index', ['posts' => Posts::orderBy('created_at','desc')->take(3)->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'catalogue_id' => 'required|numeric|exists:catalogues,id',
            'media_id' => 'numeric|exists:media,id',
        ]);

        $post = Auth::user()->posts()->create($request->all());
        $tags = $request->input('tags');
        if (isset($tags)) {
            $this->AttachTags($post, $tags);
        }
        return redirect('post/' . $post->id);
    }

    /**
     * Add tag to his post
     * @param $post
     * @param $tag_list
     */
    private function AttachTags($post, $tag_list)
    {
        $IDs = [];
        foreach ($tag_list as $tag) {
            if (is_numeric($tag)) {
                $IDs[] = $tag;
            } else {
                $IDs[] = Tags::create(['name' => $tag])->id;
            }
        }
        $post->tags()->sync($IDs);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('post.show', ['post' => Posts::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('post.edit', ['post' => Posts::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'catalogue_id' => 'required|numeric|exists:catalogues,id',
            'media_id' => 'numeric|exists:media,id',
        ]);
        $post = Auth::user()->posts()->where('id',$id)->first();
        if(is_null($post))return response('404 Page not found',404);
        $tags = $request->input('tags');
        if (isset($tags)) {
            $this->AttachTags($post, $tags);
        }
        return redirect('post/' . $post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Posts::findOrFail($id);
        if ($post->ismy()) {
            $media = $post->media;
            if (isset($media)) {
                $media->delete();
            }
            $post->delete();
            return redirect('/post')->with('done', ['title' => 'Delete Done!', 'body' => '']);
        }
        return response('Unauthorized', 401);
    }

    /** create with cataloge defult value
     * @param $id
     * @return $this
     */
    public function catalogueCreate($id)
    {
        return view('post.create')->with('select_default', $id);
    }
}
