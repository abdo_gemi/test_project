<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Socialite;
use Config;
use Validator;

class LoginWithController extends AuthController
{
    /**
     * Redirect the user to the Google authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        $with = func_get_args()[0];
        if (!$this->isAvailableApi($with)) return response('not found page', 404);
        return Socialite::driver($with)->redirect();
    }

    /**
     * @param $with
     * @return bool
     */
    private function isAvailableApi($with)
    {
        $value=(Config::get('services.' . $with));
        if (isset($value)) {
            return true;
        } else {
            false;
        }
    }

    /**
     * Obtain the user information from Google.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $with = func_get_args()[0];
        $user = Socialite::driver($with)->user();
        $his_Email = $this->loginApi($user);
        if ($his_Email) {
            return redirect('/profile');
        } else {
            return view('errors.320');
        }
    }
}