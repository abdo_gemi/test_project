<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Media;
use Auth;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    /**
     * MediaController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            if ($request->hasFile('img') || $request->hasFile('profile_upload')) {
                $this->validate($request, [
                    'img' => 'required_without:profile_upload|image|max:1024',
                    'profile_upload' => 'required_without:img|image|max:1024'
                ]);
                if ($request->hasFile('img'))
                    $media = $this->createMediaModel($request, 'img');
                else {
                    $media = $this->createMediaModel($request, 'profile_upload');
                    $delete_media_id='';
                    if (isset($request->user()->media_id)) {
                        $delete_media_id=($request->user()->media_id);
                    }
                    $request->user()->media_id = $media->id;
                    $request->user()->save();
                    $this->destroy($delete_media_id);
                }
                return response()->json(['image' => $media->attributesToArray()]);
            } else {
                return response('File not loaded', 404);
            }
        }
        return response('404  file Not found', 404);
    }

    /**
     * Make model and save image file by random name
     * @param $request
     * @param $filename
     * @return Media
     */
    private function createMediaModel(&$request, $filename)
    {
        $media = new Media();
        $file = $request->file($filename);
        $ex = $file->getClientOriginalExtension();
        // Try to make unique file name
        $media->file_name = md5(Carbon::now()->toTimeString() . Auth::user()->id . str_random(15)) . '.' . $ex;
        $media->mime_type_id = $file->getClientMimeType();
        $media->path_file = 'src/img/posts';
        $media->saveOrFail();
        $file->move($media->path_file, $media->file_name);
        return $media;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Request::capture()->ajax()) {
            $media = Media::findOrFail($id);
            return response()->json(['image' => $media->attributesToArray()]);
        } else {
            return response('404 Not found', 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media = Media::findOrFail($id);
        if (File::delete($media->full_path))
            $media->delete();
        else response('Media not found', 404);
        return response()->json(['delete' => $media->full_path]);
    }

}
