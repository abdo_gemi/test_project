<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Models\Posts;

class PostController extends ApiController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getIndex()
    {
        $paginate = Posts::with(['user', 'media', 'catalogue', 'user.media'])->orderBy('created_at', 'desc')->paginate(3, ['*'], 'p');
        $json = [];
        $json['paginate'] = $this->paginateArray($paginate);
        $json['data'] = $this->reViewsPosts($paginate->items());
        return $this->respone($json);
    }

    /**
     * @param $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    private function reViewsPosts($post)
    {
        if (!$post) return null;
        if (is_array($post)) {
            $posts = $post;
            $view = array();
            foreach ($posts as $post) {
                $view []= ''.view('post.includes.unit', ['post' => $post]);
            }
            return $view;
        } else {
            return view('post.includes.unit', ['post' => $post]);
        }
    }
}