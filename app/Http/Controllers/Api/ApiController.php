<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;

class ApiController extends Controller
{

    /**
     * @param $json_array
     * @param int $statCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respone($json_array, $statCode = 200)
    {
        return response()->json($json_array, $statCode);
    }
    
    /**
     * @param $paginateObject
     * @return mixed
     */
    protected function paginateArray($paginateObject)
    {
        $paginate = [
            'total' => $paginateObject->total(),
            'pageName' => $paginateObject->getPageName(),
            'url' => $this->getRedirectUrl(),
            'currentPage' => $paginateObject->currentPage()];
        return $paginate;
    }
}
