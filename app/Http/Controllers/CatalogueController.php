<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Catalogues;
use Illuminate\Http\Request;

class CatalogueController extends Controller
{
    /**
     * CatalogueController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
    }

    /**
     * Display a listing of the Catalogue .
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('catalogue.index')->with('catalogs', Catalogues::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('catalogue.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:catalogues|max:50',
            'description' => 'required'
        ]);
        $catalogue = Catalogues::create($request->all());
        return redirect('/catalogue/' . $catalogue->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       // $catalogue = Catalogues::with('posts')->find($id);
        return view('catalogue.show')->with('catalogue', Catalogues::with('posts')->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('catalogue.update')->with('catalogue', Catalogues::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $catalogue = Catalogues::find($id);
        $rules = [];
        if ((trim($catalogue->name) == trim($request->input('name')))) {

        } else {
            $rules = ['name' => 'required|unique:catalogues|max:50',
                'description' => 'required'];
        }
        $this->validate($request, $rules);
        $catalogue->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Catalogues::findOrFail($id)->delete();
        return redirect('/catalogue')->with('done', ['title' => 'Delete Done', 'body' => '']);
    }
}
