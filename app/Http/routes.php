<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|   resource
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['prefix' => 'api'], function () {
    Route::controller('posts', 'Api\PostController');
});
Route::auth();
Route::get('/welcome', 'HomeController@index');
Route::resource('catalogue', 'CatalogueController');
Route::resource('tag', 'TagController');
Route::resource('media', 'MediaController', ['only' => ['store', 'show', 'destroy']]);
Route::resource('post', 'PostController');
Route::resource('profile', 'ProfileController', ['only' => ['index', 'show']]);
Route::get('catalogue/createpost/{id}', 'PostController@catalogueCreate');
//Api for FaceBook ,Google ,Twitter , LinkedIn  
Route::get('auth/{id}', 'Auth\LoginWithController@redirectToProvider');
Route::get('auth/{id}/callback', 'Auth\LoginWithController@handleProviderCallback');
