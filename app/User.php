<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Add name Attribute
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * The relationship between media and users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function media()
    {
        return $this->belongsTo('App\Models\Media', 'media_id', 'id');
    }

    /**
     * Return image path
     * @return string
     */
    public function getPathImageAttribute()
    {
        if (isset($this->media)) {
            return $this->media->full_path;
        } else {
            return 'src/img/default.png';
        }
    }

    /**
     * @return mixed
     */
    public function getPostsPAttribute()
    {
        return ($this->posts()->paginate(5, ['*'], 'p'));
    }

    /**
     * The Relationship with Posts models
     *
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Posts', 'user_id', 'id');
    }

    /**
     * @param $name
     */
    public function setNameAttribute($name)
    {
        $names = explode(' ', $name);
        $this->first_name = $names[0];
        $this->last_name = $names[1];
    }
}
