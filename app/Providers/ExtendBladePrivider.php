<?php

namespace App\Providers;
use Form;
use Illuminate\Support\ServiceProvider;

/**
 * Class ExtendBladePrivider
 * @package App\Providers
 */
class ExtendBladePrivider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
          $this->addBstextField();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * @return var void
     *  Add Form::bstext in Views
     */
    public function addBstextField()
    {
        Form::component('bsText','includes.form_field.bstext', ['name', 'value' => null,'label'=>'','attributes' => []]);
    }
    

}
