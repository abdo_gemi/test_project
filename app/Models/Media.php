<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Media
 * @package App\Models
 */
class Media extends Model
{
    /**
     * table name in database
     *
     * @var string
     */
    protected $table = 'media';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_name',
        'path_file',
        'mime_type_id'
    ];
    /**
     * Mime type form media
     * @var array
     */
    protected $mime_type=[
        1=>'image/png',
        2=>'image/jpeg'
    ];


    /**
     * This model don't has created_at or Updated_at
     * @var bool
     */
    public $timestamps = false;

    /**
     * set id form array $mine_type
     * @param $type
     * @return mixed
     */
    public function setMimeTypeIdAttribute($type)
    {
        $this->attributes['mime_type_id']=array_flip($this->mime_type)[strtolower($type)];
    }

    /**
     *
     * @param $id
     * @return mixed
     */
    public function getMimeTypeAttribute($id)
    {
        return $this->mime_type[$id];
    }
    /**
     *
     *get full path attribute
     * @return mixed
     */
    public function getFullPathAttribute()
    {
        return $this->path_file."/".$this->file_name;
    }
}
