<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tags
 * @package App\Models
 */
class Tags extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable=[
        'name'];
    /**
     * table name in database
     * @var string
     */
    protected $table='tags';

    /**
     * Relationship between Posts and User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
      return  $this->belongsToMany('App\Models\Posts','tag_post','tag_id','post_id');
    }

    /**
     * @return mixed
     */
    public function getPostsPAttribute()
    {
        return ($this->posts()->paginate(5,['*'],'p'));
    }


}
