<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Catalogues
 * @package App\Models
 */
class Catalogues extends Model
{
    /**
     * @var string
     */
    protected $table = 'catalogues';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description'];

    /**
     * Relationship in Catalogue and Posts
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Posts', 'catalogue_id', 'id');
    }

    /**
     * @return mixed
     */
    public function getPostsPAttribute()
    {
      return ($this->posts()->paginate(5,['*'],'p'));
    }
}
