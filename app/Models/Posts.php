<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class Posts
 * @package App\Models
 */
class Posts extends Model
{


    /**
     * @var string
     */
    protected $table = 'posts';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'catalogue_id',
        'body',
        'media_id'
    ];
    protected $dates=['created_at','updated_at'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Get Tags which his related with Post
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tags','tag_post','post_id','tag_id');
    }
    /**
     * Get catalogue which his related with Post
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function catalogue()
    {
        return $this->belongsTo('App\Models\Catalogues', 'catalogue_id', 'id');
    }
    /**
     * Get media which his related with Post
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function media()
    {
        return $this->belongsTo('App\Models\Media', 'media_id', 'id');
    }

    /**
     * Return true if is the owner the post
     * @return bool
     */
    public function ismy()
    {
        return Auth::user()->id == $this->user_id;
    }
    

}
