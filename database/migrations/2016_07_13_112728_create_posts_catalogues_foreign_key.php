<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsCataloguesForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->foreign('catalogue_id')->references('id')->on('catalogues')->onDelete('cascade');
            $table->foreign('media_id')->references('id')->on('media')->onDelete('cascade');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('media_id')->references('id')->on('media');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign('posts_media_id_foreign');
            $table->dropForeign('posts_catalogue_id_foreign');
            //$table->dropForeign('users_media_id_foreign');

        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_media_id_foreign');

        });
    }
}
