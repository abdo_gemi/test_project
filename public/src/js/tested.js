/**
 * Created by gami on 14/07/16.
 */
window.onload = function () {
    var str = (window.location.toString().split(window.location.host)[1]);
    str = (str.split('/')[1]);
    document.getElementById(str).setAttribute('class', 'active');
}
$(function () {
    var model='<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"'+
    'aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header">'+
    '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>'+
    '</button><h4 class="modal-title" id="myModalLabel">Delete confirmation</h4></div><div class="modal-body">'+
        'Are you sure for deleting this ?</div><div class="modal-footer">'+
        '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>'+
        '<button id="delete_button" type="button" onclick="document.getElementById(\'delete\').submit();" class="btn btn-danger">' +
        'Delete</button></div> </div> </div> </div>';
    $('body').append(model);
});