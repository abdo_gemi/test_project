/**
 * @return void
 * @param input Object element
 */
function UploadImage(inputObject) {
    var data = new FormData();
    $.each(inputObject.files, function (k, value) {
        data.append(inputObject.name, value);
    });
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
            url: '/media',
            type: 'POST',
            contentType: false,
            processData: false,
            dataType: 'json',
            data: data,
            statusCode: {
            422: function(data) {
                $( "#button_upload" ).after( alertMessage(data.responseJSON.img) );
                document.getElementById("button_upload").value = "";
            }
        }
        }
    ).done(function (json) {
        displayImage(inputObject,json.image);
        window.onbeforeunload = function (e) {
            return false;
        };
        window.onunload = function (e) {
            UnloadImage(this,json.image.id)
        }
    })
        .fail(function (xhr, status, errorThrown) {
        alert("Sorry, there was a problem!");
    });
}
/**
 * @return {string} html form img
 * @param image object json
 */
function showImage(image) {
    var imgTag = '<img src="/' + image.path_file + '/' + image.file_name + '" class="img-thumbnail" alt="Uploaded image"id="uploaded-img" accept="image/*"/>';
    return  '<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"><div class="thumbnail">' +
        '<button type="button" class="close" onclick="UnloadImage(this,'+image.id+')">&times;</button>' + imgTag + '</div></div>';
}
/**
 * @return void
 * @param object of  Element and img_id
 */
function UnloadImage(object,img_id) {
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax(
        {
            url: '/media/' + img_id,
            type: 'DELETE',
            processData: false,
            dataType: 'json',
            data: {},
        }
    ).done(function (json) {
        object.parentElement.parentElement.parentElement.innerHTML='<input type="file" class="btn-file"'
            +'name="img" id="button_upload"'
            +'onchange="UploadImage(this)"/>';
        window.onunload = function (e) {
        };
        window.onbeforeunload = function (e) {
        };
    }).fail(function (xhr, status, errorThrown) {
        alert("Sorry some error in delete image");
    });
};
$(function() {
    $('#postForm').submit(function() {

        window.onbeforeunload = function (e) {
        };
        window.onunload = function (e) {
        }
        return true; // return false to cancel form action
    });
});
function alertMessage(error) {
        var message="<ul>";
        if( Object.prototype.toString.call( error ) === '[object Array]' ) {
            for(var index=0;index<error.length;index++)
            {
                message+='<li>'+error[index]+'</li>';
            }
        }
        else
        {
            message+='<li>'+error+'</li>';
        }
        return  '<div class="alert alert-danger">'+
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;'+
            '</button>'+message+'</ul></div>';

    }
function displayImage(inputObject,image) {
    console.dir(image);
    var img=showImage(image);
    img+='<input type="hidden" name="media_id"value="'+image.id+'">'
    inputObject.parentElement.innerHTML=img;
}
function uploadImageProfile(inputObject) {
    var data = new FormData();
    $.each(inputObject.files, function (k, value) {
        data.append(inputObject.name, value);
    });
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
    $.ajax({
            url: '/media',
            type: 'POST',
            contentType: false,
            processData: false,
            dataType: 'json',
            data: data,
            statusCode: {
                422: function(data) {
                    $( "#update-image" ).after( alertMessage(data.responseJSON.img) );
                    document.getElementById("#update-image").value = "";
                }
            }
        }
    ).done(function (json) {
            alert("Done!");
            $('img#profile-uploaded').attr('src','/' + json.image.path_file + '/' +json.image.file_name);
    })
        .fail(function (xhr, status, errorThrown) {
            alert("Sorry, there was a problem!");
        });
}